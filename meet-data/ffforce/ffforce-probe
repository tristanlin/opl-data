#!/usr/bin/env python3
# vim: set ts=8 sts=4 et sw=4 tw=99:
#
# Probes for new Fédération Française de Force meets.

from bs4 import BeautifulSoup
import os
import sys
import requests

try:
    import oplprobe
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(os.path.dirname(
        os.path.dirname(os.path.realpath(__file__)))), "scripts"))
    import oplprobe


RESULTS_URL = ("https://www.ffforce.fr/fr/nos-disciplines/"
               "force-athletique-ffforce/competitions-force-athletique/"
               "archives-des-resultats-force-athletique.html")
BASEURL = "https://www.ffforce.fr"
FEDDIR = os.path.dirname(os.path.realpath(__file__))


def color(s):
    return "\033[1;37m" + s + "\033[0;m"


def getmeetlist(html):
    soup = BeautifulSoup(html, 'html.parser')

    # get the value attributes for the two most recent seasons in the season dropdown
    # then make POST requests
    season_selector = soup.find('select')
    options = season_selector.find_all('option')
    last_two_seasons = options[1:3]
    last_two_seasons_values = [season['value'] for season in last_two_seasons]

    output = []

    for season_value in last_two_seasons_values:

        results = requests.post(RESULTS_URL, {'jsArtSelectCategorie': season_value})
        soup_results = BeautifulSoup(results.text, 'html.parser')

        main = soup_results.find("div", "tableau-classmt")
        assert main, "No <div> element with class `tableau-classmt` found."

        links = main.find_all("div", "cel-classmt site")
        assert links, "No <div> element with class `cel-classmt site` found."

        full_links = list(map(lambda link: BASEURL + link.find('a')['href'], links))

        output.extend(full_links)

    return output


def main():
    html_content = oplprobe.gethtml(RESULTS_URL)
    meets_list = getmeetlist(html_content)

    entered = oplprobe.getenteredurls(FEDDIR)
    unentered = oplprobe.getunenteredurls(meets_list, entered)

    oplprobe.print_meets(color('[FFForce]'), unentered)


if __name__ == '__main__':
    main()
